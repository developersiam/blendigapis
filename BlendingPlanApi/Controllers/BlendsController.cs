﻿using BlendingPlanApi.Models;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using DomainModel;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class BlendsController : ApiController
    {
        // GET: api/Blends
        public IHttpActionResult Get(int crop, string type)
        {
            try
            {
                var blendColumn = BusinessLayerService.BlendingPlanBL().GetCustomerBlendColumn(crop, type);
                var blendRowHeader = BusinessLayerService.BlendingPlanBL().GetCustomerBlendRowHeader(crop, type);

                List<Blend> blendList = new List<Blend>();
                foreach (var item in blendColumn)
                {
                    var rowHeader = blendRowHeader.Single(b => b.crop == item.crop 
                        && b.type == item.type 
                        && b.classify == item.classify);

                    blendList.Add(new Blend
                    {
                        blendId = item.cusBlendId,
                        custSpecID = item.cusSpectId,
                        cropSizeDetailId = rowHeader == null ? null : rowHeader.cropSizeDetailID,
                        crop = item.crop,
                        type = item.type,
                        classify = item.classify,
                        customer = item.customer,
                        packedGrade = item.packedGrade,
                        green = 0,
                        blendPercent = item.blendPercent
                    });
                }

                List<CustomerBlend> cusBlends = new List<CustomerBlend>();
                foreach (var item in blendRowHeader)
                {
                    cusBlends.Add(new CustomerBlend
                    {
                        crop = Convert.ToInt16(item.crop),
                        type = item.type,
                        classify = item.classify,
                        cropThrow = Convert.ToDecimal(item.cropThrow),
                        stock = item.stock,
                        actualBought = item.actualBought,
                        accumulativeRemainningBlending = 0,
                        cropSizeDetailId = item.cropSizeDetailID,
                        cropSizeId = item.cropSizeID.GetValueOrDefault(),
                        blends = blendList.Count <= 0 ? null : blendList.Where(bl => bl.crop == item.crop && bl.type == item.type && bl.classify == item.classify).ToList()
                    });
                }

                return Ok(cusBlends);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Blends/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Blends
        [HttpPost]
        public IHttpActionResult Post([FromBody]List<CustomerBlend> value)
        {
            try
            {
                // Manage data in crop size detail. ***********************************************************************
                // ********************************************************************************************************
                // Add list of crop throw.
                List <CustomerBlend> cropThrowAddList = new List<CustomerBlend>();
                cropThrowAddList = value.Where(c => c.cropSizeDetailId == null 
                && c.cropThrow != 0).ToList();
                if (cropThrowAddList.Count > 0)
                {
                    BusinessLayerService.CropSizeDetailBL()
                        .AddCropSizeDetail(cropThrowAddList.Select(c => new BD_CropSizeDetail
                    {
                        CropSizeDetailID = Guid.NewGuid(),
                        Classify = c.classify,
                        CropThrow = c.cropThrow,
                        CropSizeID = c.cropSizeId,
                        CreateDate = DateTime.Now,
                        CreateBy = "",
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = ""
                    }).ToList());
                }

                // Update list of crop throw.
                List<CustomerBlend> cropThrowEditList = new List<CustomerBlend>();
                cropThrowEditList = value.Where(c => c.cropSizeDetailId != null).ToList();
                if (cropThrowEditList.Count > 0)
                {
                    BusinessLayerService.CropSizeDetailBL()
                        .EditCropSizeDetail(cropThrowEditList.Select(c => new BD_CropSizeDetail
                    {
                        CropSizeDetailID = c.cropSizeDetailId.GetValueOrDefault(),
                        Classify = c.classify,
                        CropThrow = c.cropThrow,
                        CropSizeID = c.cropSizeId,
                        CreateDate = DateTime.Now,
                        CreateBy = "",
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = ""
                    }).ToList());
                }

                //// Delete list of crop throw.
                //List<CustomerBlend> cropThrowDeleteList = new List<CustomerBlend>();
                //cropThrowDeleteList = value.Where(c => c.cropSizeDetailId != null 
                //&& c.cropThrow == 0).ToList();
                //if (cropThrowDeleteList.Count > 0)
                //{
                //    BusinessLayerService.CropSizeDetailBL()
                //        .DeleteCropSizeDetail(cropThrowDeleteList
                //        .Select(c => new BD_CropSizeDetail
                //    {
                //        CropSizeDetailID = c.cropSizeDetailId.GetValueOrDefault(),
                //        Classify = c.classify,
                //        CropThrow = c.cropThrow,
                //        CropSizeID = c.cropSizeId,
                //        CreateDate = DateTime.Now,
                //        CreateBy = "",
                //        ModifiedDate = DateTime.Now,
                //        ModifiedBy = ""
                //    }).ToList());
                //}

                // Manage data in customer blend. ************************************************************************
                // ********************************************************************************************************
                List<Blend> blendList = new List<Blend>();
                foreach (var item in value)
                {
                    blendList.AddRange(item.blends);
                }

                // Add list of customer blend percent
                List<BD_CustomerBlend> list = new List<BD_CustomerBlend>();
                list = BusinessLayerService.BlendingPlanBL()
                    .GetCustomerBlend(blendList.FirstOrDefault().crop, 
                    blendList.FirstOrDefault().type).ToList();

                List<Blend> blendAddList = new List<Blend>();
                blendAddList = blendList.Where(b => b.blendId == null 
                && b.blendPercent != 0).ToList();

                if (blendAddList.Count > 0)
                {
                    BusinessLayerService.BlendingPlanBL()
                        .AddCustomerBlend(blendAddList.Select(b => new BD_CustomerBlend
                    {
                        CustBlendID = Guid.NewGuid(),
                        BlendPercent = Convert.ToDecimal(b.blendPercent),
                        CropSizeDetID = b.cropSizeDetailId.GetValueOrDefault(),
                        CustSpecID = b.custSpecID,
                        CreateBy = "",
                        CreateDate = DateTime.Now,
                        ModifiedBy = "",
                        ModifiedDate = DateTime.Now
                    }).ToList());
                }

                // Update list of customer blend percent
                List<Blend> blendEditList = new List<Blend>();
                blendEditList = blendList.Where(b => b.blendId != null 
                && b.blendPercent != 0).ToList();
                if (blendEditList.Count > 0)
                {
                    BusinessLayerService.BlendingPlanBL()
                        .EditCustomerBlend(blendEditList.Select(b => new BD_CustomerBlend
                    {
                        CustBlendID = b.blendId.GetValueOrDefault(),
                        BlendPercent = Convert.ToDecimal(b.blendPercent),
                        CropSizeDetID = b.cropSizeDetailId.GetValueOrDefault(),
                        CustSpecID = b.custSpecID,
                        CreateBy = "",
                        CreateDate = DateTime.Now,
                        ModifiedBy = "",
                        ModifiedDate = DateTime.Now
                    }).ToList());
                }

                // Delete list of customer blend percent
                List<Blend> blendDeleteList = new List<Blend>();
                blendDeleteList = blendList.Where(b => b.blendId != null 
                && b.blendPercent == 0).ToList();
                if (blendDeleteList.Count > 0)
                {
                    BusinessLayerService.BlendingPlanBL()
                        .DeleteCustomerBlend(blendDeleteList.Select(b => new BD_CustomerBlend
                    {
                        CustBlendID = b.blendId.GetValueOrDefault(),
                        BlendPercent = Convert.ToDecimal(b.blendPercent),
                        CropSizeDetID = b.cropSizeDetailId.GetValueOrDefault(),
                        CustSpecID = b.custSpecID,
                        CreateBy = "",
                        CreateDate = DateTime.Now,
                        ModifiedBy = "",
                        ModifiedDate = DateTime.Now
                    }).ToList());
                }
                return Ok(value);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
