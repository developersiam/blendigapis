﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CropSizesController : ApiController
    {
        // GET: api/CropSizes
        public IHttpActionResult Get()
        {
            return Ok(BusinessLayerService.CropSizeBL()
                .GetAllCropSize()
                .Select(c => new CropSize
                {
                    id = c.CropSizeID,
                    crop = c.Crop,
                    type = c.TobaccoType,
                    cropSize = c.CropSize,
                    createBy = c.CreateBy,
                    createDate = c.CreateDate,
                    modifiedBy = c.ModifiedBy,
                    modifiedDate = c.ModifiedDate
                }));
        }

        public IHttpActionResult Get(int crop, string type)
        {
            var item = BusinessLayerService.CropSizeBL().GetCropSizeByCropAndType(crop, type);

            if (item == null)
                return NotFound();

            return Ok(new CropSize
            {
                id = item.CropSizeID,
                crop = item.Crop,
                type = item.TobaccoType,
                cropSize = item.CropSize,
                createBy = item.CreateBy,
                createDate = item.CreateDate,
                modifiedBy = item.ModifiedBy,
                modifiedDate = item.ModifiedDate
            });
        }

        // POST: api/CropSizes
        public IHttpActionResult Post(CropSize value)
        {
            try
            {
                if (BusinessLayerService.CropSizeBL().GetAllCropSize()
                    .Where(cs => cs.Crop == value.crop
                    && cs.TobaccoType == value.type).Count() > 0)
                    return BadRequest("Dupplicated value.");

                BusinessLayerService.CropSizeBL().AddCropSize(new BD_CropSize
                {
                    CropSizeID = Guid.NewGuid(),
                    Crop = value.crop,
                    TobaccoType = value.type,
                    CropSize = value.cropSize,
                    CreateBy = User.Identity.Name,
                    CreateDate = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/CropSizes/5
        [HttpPut]
        public IHttpActionResult Put(Guid id, CropSize value)
        {
            try
            {
                var _cropSize = BusinessLayerService.CropSizeBL().GetCropSizeByID(id);

                if (_cropSize == null)
                    return NotFound();

                _cropSize.CropSize = value.cropSize;
                _cropSize.ModifiedBy = User.Identity.Name;
                _cropSize.ModifiedDate = DateTime.Now;

                BusinessLayerService.CropSizeBL().EditCropSize(_cropSize);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/CropSizes/5
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                if (BusinessLayerService.CropSizeBL().GetCropSizeByID(id) == null)
                    return NotFound();

                BusinessLayerService.CropSizeBL().DeleteCropSize(id);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
