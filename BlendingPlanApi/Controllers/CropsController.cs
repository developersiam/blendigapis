﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BlendingPlanApi.Models;
using BusinessLayer;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/crops")]
    public class CropsController : ApiController
    {
        [HttpGet]
        [Route("GetDefaultCrop")]
        public IHttpActionResult GetDefaultCrop()
        {
            var model = BusinessLayerService.CropBL().GetDefaultCrop();
            if (model == null)
                return NotFound();

            return Ok(new Crop
            {
                crop = model.Crop,
                defaultStatus = model.DefaultCrop
            });
        }

        // GET: api/Crops
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(BusinessLayerService.CropBL()
                .GetCrops()
                .Select(p => new Crop
                {
                    crop = p.Crop,
                    defaultStatus = p.DefaultCrop
                }));
        }

        // GET: api/Crops/5
        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            var _crop = BusinessLayerService.CropBL().GetCrop(id);

            if (_crop == null)
                return NotFound();
            else
                return Ok(new Crop
                {
                    crop = _crop.Crop,
                    defaultStatus = _crop.DefaultCrop
                });
        }

        // POST: api/Crops
        [HttpPost]
        public IHttpActionResult Post()
        {
            BusinessLayerService.CropBL().AddCrop(User.Identity.Name);
            return Ok();
        }

        // PUT: api/Crops/5
        [HttpPut]
        public IHttpActionResult Put(short id)
        {
            var _crop = BusinessLayerService.CropBL().GetCrop(id);
            if (_crop == null)
                return NotFound();

            BusinessLayerService.CropBL().SetDefaultCrop(id);
            return Ok();
        }

        // DELETE: api/Crops/5
        [HttpDelete]
        public IHttpActionResult Delete(short id)
        {
            var _crop = BusinessLayerService.CropBL().GetCrop(id);
            if (_crop == null)
                return NotFound();

            BusinessLayerService.CropBL().DeleteCrop(_crop.Crop);
            return Ok();
        }
    }
}
