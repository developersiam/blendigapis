﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CustomerBlendFiltersController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int crop, string type)
        {
            try
            {
                List<CustomerBlendFilter> list = new List<CustomerBlendFilter>();
                list = BusinessLayerService.CustomerBlendFilterBL()
                    .GetByCropAndType(crop, type).Select(cb => new CustomerBlendFilter
                    {
                        crop = cb.BD_CropSizeDetail.BD_CropSize.Crop,
                        type = cb.BD_CropSizeDetail.BD_CropSize.TobaccoType,
                        classify = cb.BD_CropSizeDetail.Classify,
                        cropSizeDetailId = cb.CropSizeDetailID
                    }).ToList();

                return Ok(list);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Post(CustomerBlend model)
        {
            try
            {
                BusinessLayerService.CustomerBlendFilterBL().Add(model.cropSizeDetailId.GetValueOrDefault());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                BusinessLayerService.CustomerBlendFilterBL().Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
