﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CustomerSpecFiltersController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int crop, string type)
        {
            try
            {
                return Ok(BusinessLayerService.CustomerSpecFilterBL()
                    .GetByCropAndType(crop, type).Select(cs => new CustomerSpecFilter
                    {
                        crop = cs.BD_CustomerSpec.Crop,
                        type = cs.BD_CustomerSpec.TobaccoType,
                        customer = cs.BD_CustomerSpec.CustomerCode,
                        packedGrade = cs.BD_CustomerSpec.PackedGrade,
                        customerSpecId = cs.CustomerSpecID
                    }));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Post(CustomerSpec model)
        {
            try
            {
                BusinessLayerService.CustomerSpecFilterBL().Add(model.id.GetValueOrDefault());
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                BusinessLayerService.CustomerSpecFilterBL().Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
