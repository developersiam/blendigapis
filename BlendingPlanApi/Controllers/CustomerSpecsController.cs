﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CustomerSpecsController : ApiController
    {
        // GET: api/CustomerSpecs
        public IHttpActionResult Get(int crop, string type)
        {
            try
            {
                return Ok(BusinessLayerService.BlendingPlanBL().GetPackedToDate(crop, type)
                    .Select(cs => new CustomerSpec
                    {
                        id = cs.custSpecID,
                        crop = cs.crop,
                        type = cs.type,
                        customer = cs.customer,
                        packedGrade = cs.packedGrade,
                        netKg = cs.netKg,
                        orderCase = cs.orderCase,
                        packedCases = cs.packedCases,
                        actualGreen = cs.actualGreen,
                        actualYield = cs.actualYield,
                        manualYield = cs.manualYield,
                        packedStockKg = cs.packedStockKg,
                        packedStockCases = cs.packedStockCases,
                        accumulativeBlenPercent = 0,
                        accumulativeGreenEstimate = 0
                    }));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/CustomerSpecs
        public IHttpActionResult Post(CustomerSpec value)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                BusinessLayerService.CustomerSpecBL().AddCustomerSpec(new BD_CustomerSpec
                {
                    Crop = value.crop,
                    TobaccoType = value.type,
                    CustomerCode = value.customer,
                    PackedGrade = value.packedGrade,
                    OrderCase = value.orderCase,
                    NetKg = value.netKg,
                    YieldManual = Convert.ToDecimal(value.manualYield),
                    CreateBy = User.Identity.Name
                });
                return Ok(value);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/customerSpecs/edit/{id}")]
        [HttpPut]
        public IHttpActionResult Edit(Guid id, CustomerSpec value)
        {
            try
            {
                var model = BusinessLayerService.CustomerSpecBL().GetCustomerSpecById(id);
                if (model == null)
                    return NotFound();

                model.CustomerCode = value.customer;
                model.PackedGrade = value.packedGrade;
                model.OrderCase = value.orderCase;
                model.NetKg = value.netKg;
                model.YieldManual = Convert.ToDecimal(value.manualYield);
                model.ModifiedBy = User.Identity.Name;

                BusinessLayerService.CustomerSpecBL().EditCustomerSpecByItem(model);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        // PUT: api/CustomerSpecs/5
        public IHttpActionResult Put(List<CustomerSpec> value)
        {
            try
            {
                if (value.Count > 0)
                {
                    BusinessLayerService.CustomerSpecBL().EditCustomerSpec(value.Select(cs => new BD_CustomerSpec
                    {
                        CustSpecID = cs.id.GetValueOrDefault(),
                        Crop = cs.crop,
                        TobaccoType = cs.type,
                        CustomerCode = cs.customer,
                        PackedGrade = cs.packedGrade,
                        OrderCase = cs.orderCase,
                        NetKg = cs.netKg,
                        YieldManual = Convert.ToDecimal(cs.manualYield),
                        ModifiedBy = "Eakkaluck"
                    }).ToList());
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/CustomerSpecs/5
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                BusinessLayerService.CustomerSpecBL().DeleteCustomerSpec(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
