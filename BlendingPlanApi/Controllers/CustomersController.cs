﻿using BlendingPlanApi.Models;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class CustomersController : ApiController
    {
        // GET: api/Customers
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(BusinessLayerService.CustomerBL().GetCustomers()
                    .Select(x => new Customer
                    {
                        code = x.code,
                        name = x.name
                    }));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
