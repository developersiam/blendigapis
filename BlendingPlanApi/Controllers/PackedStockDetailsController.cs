﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class PackedStockDetailsController : ApiController
    {
        // GET: api/PackedStockDetails
        public IHttpActionResult Get(Guid cusSpecId)
        {
            try
            {
                return Ok(BusinessLayerService.PackedStockDetailBL()
                    .GetListByCustomerSpec(cusSpecId).Select(p => new PackedStockDetail
                    {
                        id = p.PackedStockDetailID,
                        packedCasesNetKgID = p.PackedCasesNetKgID,
                        netKg = p.BD_PackedCasesNetKg.NetKg,
                        packedCases = p.PackedCases,
                        custSpecID = p.CustSpecID,
                        createBy = p.CreateBy,
                        createDate = p.CreateDate,
                        modifiedBy = p.ModifiedBy,
                        modifiedDate = p.ModifiedDate
                    }).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/PackedStockDetails
        public IHttpActionResult Post(PackedStockDetail value)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (BusinessLayerService.PackedStockDetailBL().IsDupplicate(value.packedCasesNetKgID, value.custSpecID) == true)
                    return BadRequest("Dupplicated data.");

                BusinessLayerService.PackedStockDetailBL().Add(new BD_PackedStockDetail
                {
                    PackedStockDetailID = Guid.NewGuid(),
                    PackedCasesNetKgID = value.packedCasesNetKgID,
                    CustSpecID = value.custSpecID,
                    PackedCases = value.packedCases,
                    CreateBy = User.Identity.Name,
                    CreateDate = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/PackedStockDetails/5
        public IHttpActionResult Put(Guid id, PackedStockDetail value)
        {
            try
            {
                var model = BusinessLayerService.PackedStockDetailBL().GetSingle(id);
                if (model == null)
                    return NotFound();

                model.PackedCases = value.packedCases;
                model.ModifiedBy = User.Identity.Name;
                model.ModifiedDate = DateTime.Now;

                BusinessLayerService.PackedStockDetailBL().Edit(model);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/PackedStockDetails/5
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                var model = BusinessLayerService.PackedStockDetailBL().GetSingle(id);
                if (model == null)
                    return NotFound();

                BusinessLayerService.PackedStockDetailBL().Delete(id);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
