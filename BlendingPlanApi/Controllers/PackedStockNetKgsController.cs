﻿using BlendingPlanApi.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class PackedStockNetKgsController : ApiController
    {
        // GET: api/PackedStockNetKgs
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(BusinessLayerService.PackedStockNetKgBL()
                    .GetAll().Select(p => new PackedCasesNetKg
                    {
                        id = p.PackedCasesNetKgID,
                        netKg = p.NetKg,
                        createBy = p.CreateBy,
                        createDate = p.CreateDate,
                        modifiedBy = p.ModifiedBy,
                        modifiedDate = p.ModifiedDate
                    }));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/PackedStockNetKgs/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PackedStockNetKgs
        public IHttpActionResult Post(PackedCasesNetKg value)
        {
            try
            {
                if (BusinessLayerService.PackedStockNetKgBL().IsDupplicate(value.netKg) == true)
                    return BadRequest("Dupplicated data.");

                BusinessLayerService.PackedStockNetKgBL().Add(new BD_PackedCasesNetKg
                {
                    PackedCasesNetKgID = Guid.NewGuid(),
                    NetKg = value.netKg,
                    CreateBy = User.Identity.Name,
                    CreateDate = DateTime.Now,
                    ModifiedBy = User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/PackedStockNetKgs/5
        public IHttpActionResult Put(Guid id, PackedCasesNetKg value)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (BusinessLayerService.PackedStockNetKgBL().IsDupplicate(value.netKg) == true)
                    return BadRequest("Dupplicated data.");

                var model = BusinessLayerService.PackedStockNetKgBL().Get(id);

                if (model == null)
                    return NotFound();

                model.NetKg = value.netKg;
                model.ModifiedBy = User.Identity.Name;
                model.ModifiedDate = DateTime.Now;

                BusinessLayerService.PackedStockNetKgBL().Edit(model);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/PackedStockNetKgs/5
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                var model = BusinessLayerService.PackedStockNetKgBL().Get(id);
                if (model == null)
                    return NotFound();

                BusinessLayerService.PackedStockNetKgBL().Delete(model.PackedCasesNetKgID);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
