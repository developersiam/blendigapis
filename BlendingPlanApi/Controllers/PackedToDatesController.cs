﻿using BlendingPlanApi.Models;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class PackedToDatesController : ApiController
    {
        // GET: api/PackedToDates
        public IHttpActionResult Get(int crop, string type)
        {
            try
            {
                return Ok(BusinessLayerService.PackedToDateBL()
                    .GetPackToDateByCropAndType(crop, type)
                    .Select(p => new PackedToDate
                    {
                        crop = p.crop,
                        type = p.type,
                        customer = p.customer,
                        packedGrade = p.packedGrade,
                        netKg = p.netKg,
                        orderCase = p.orderCase,
                        actualGreen = p.actualGreen,
                        packedCases = p.packedCases,
                        packedKg = Convert.ToDecimal(p.packedKg),
                        actualYield = Convert.ToDecimal(p.actualYield),
                        manualYield = Convert.ToDecimal(p.manualYield),
                        packedStockKg = p.packedStockKg,
                        packedStockCases = p.packedStockCases,
                        casePerHour = p.casePerHour
                    }));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/PackedToDates/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PackedToDates
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PackedToDates/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PackedToDates/5
        public void Delete(int id)
        {
        }
    }
}
