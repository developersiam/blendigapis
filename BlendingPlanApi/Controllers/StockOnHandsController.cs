﻿using BlendingPlanApi.Models;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class StockOnHandsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get(int crop, string type)
        {
            return Ok(BusinessLayerService.StockOnHandBL()
                .GetStockOnHand(crop, type).ToList()
                .Select(x => new StockOnHand
                {
                    type = x.type,
                    classify = x.classify,
                    level = x.level,
                    weight = x.weight
                }));
        }
    }
}
