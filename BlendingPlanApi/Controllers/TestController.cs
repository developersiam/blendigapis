﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BlendingPlanApi.Controllers
{
    public class TestController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok("Hi!");
        }

        [Authorize]
        public IHttpActionResult Get(string str)
        {
            return Ok("Hi! " + str);
        }
    }
}
