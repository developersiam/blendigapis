﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BlendingPlanApi.Models;
using BusinessLayer;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class TobaccoTypesController : ApiController
    {
        // GET: api/TobaccoTypes
        public IHttpActionResult Get()
        {
            return Ok(BusinessLayerService.TypeBL().GetTypes().Select(t => new TobaccoType
            {
                type = t.type1,
                desc = t.desc,
                dtrecord = t.dtrecord,
                user = t.user
            }));
        }
    }
}
