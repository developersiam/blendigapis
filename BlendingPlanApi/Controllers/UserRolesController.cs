﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BusinessLayer;
using DomainModel;
using BlendingPlanApi.Models;

namespace BlendingPlanApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class UserRolesController : ApiController
    {
        public IHttpActionResult Get(string username)
        {
            try
            {
                return Ok(BusinessLayerService.UserRoleBL().GetRoles(username)
                    .Select(x => new UserRole { roleName = x.BD_Role.RoleName }).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
