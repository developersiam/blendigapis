﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class Blend
    {
        public int crop { get; set; }
        public string type { get; set; }
        public string classify { get; set; }
        public string customer { get; set; }
        public string packedGrade { get; set; }
        public decimal? blendPercent { get; set; }
        public decimal? green { get; set; }
        public Guid? blendId { get; set; }
        public Guid custSpecID { get; set; }
        public Guid? cropSizeDetailId { get; set; }
    }
}