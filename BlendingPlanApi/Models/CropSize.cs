﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class CropSize
    {
        public Guid id { get; set; }
        public int crop { get; set; }
        public string type { get; set; }
        public decimal cropSize { get; set; }
        public DateTime createDate { get; set; }
        public string createBy { get; set; }
        public DateTime modifiedDate { get; set; }
        public string modifiedBy { get; set; }
    }
}