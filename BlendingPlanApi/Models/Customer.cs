﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class Customer
    {
        public string code { get; set; }
        public string name { get; set; }
    }
}