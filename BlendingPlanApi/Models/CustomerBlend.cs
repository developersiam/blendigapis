﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class CustomerBlend
    {
        public int crop { get; set; }
        public string type { get; set; }
        public string classify { get; set; }
        public decimal cropThrow { get; set; }
        public decimal stock { get; set; }
        public decimal actualBought { get; set; }
        public decimal accumulativeRemainningBlending { get; set; }
        public Guid? cropSizeDetailId { get; set; }
        public Guid cropSizeId { get; set; }
        public List<Blend> blends { get; set; }
    }
}