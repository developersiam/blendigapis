﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class CustomerBlendFilter
    {
        public int crop { get; set; }
        public string type { get; set; }
        public string classify { get; set; }
        public Guid id { get; set; }
        public Guid cropSizeDetailId { get; set; }
    }
}