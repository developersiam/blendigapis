﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class CustomerSpec
    {
        public Guid? id { get; set; }
        public int crop { get; set; }
        public string type { get; set; }
        public string customer { get; set; }
        public string packedGrade { get; set; }
        public decimal netKg { get; set; }
        public int orderCase { get; set; }
        public int? packedCases { get; set; }
        public decimal? actualGreen { get; set; }
        public decimal? actualYield { get; set; }
        public decimal? manualYield { get; set; }
        public int? packedStockCases { get; set; }
        public decimal? packedStockKg { get; set; }
        public decimal? accumulativeBlenPercent { get; set; }
        public decimal? accumulativeGreenEstimate { get; set; }
    }
}