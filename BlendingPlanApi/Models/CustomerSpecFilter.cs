﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class CustomerSpecFilter
    {
        public Guid id { get; set; }
        public int crop { get; set; }
        public string type { get; set; }
        public string customer { get; set; }
        public string packedGrade { get; set; }
        public Guid customerSpecId { get; set; }
    }
}