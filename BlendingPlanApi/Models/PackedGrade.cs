﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class PackedGrade
    {
        public string packedgrade { get; set; }
        public short crop { get; set; }
        public string type { get; set; }
        public string customer { get; set; }
        public string form { get; set; }
        public decimal netdef { get; set; }
        public decimal taredef { get; set; }
        public decimal grossdef { get; set; }
        public decimal gepbaht { get; set; }
        public string packingmat { get; set; }
        public DateTime dtrecord { get; set; }
        public string pduser { get; set; }
        public string redrycode { get; set; }
        public string transportcode { get; set; }
    }
}