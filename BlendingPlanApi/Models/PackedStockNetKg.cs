﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class PackedStockNetKg
    {
        public System.Guid id { get; set; }
        public decimal netKg { get; set; }
        public System.DateTime createDate { get; set; }
        public string createBy { get; set; }
        public System.DateTime modifiedDate { get; set; }
        public string modifiedBy { get; set; }
    }
}