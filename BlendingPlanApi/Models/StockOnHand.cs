﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class StockOnHand
    {
        public string classify { get; set; }
        public Nullable<int> level { get; set; }
        public string type { get; set; }
        public decimal weight { get; set; }
    }
}