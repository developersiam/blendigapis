﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlendingPlanApi.Models
{
    public class TobaccoType
    {
        public string type { get; set; }
        public string desc { get; set; }
        public DateTime? dtrecord { get; set; }
        public string user { get; set; }
    }
}