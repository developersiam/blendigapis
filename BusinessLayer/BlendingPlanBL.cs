﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface IBlendingPlanBL
    {
        List<BD_CustomerBlend> GetCustomerBlend(int crop, string type);
        List<sp_BP_Sel_CustomerBlendColumn_Result> GetCustomerBlendColumn(int crop, string type);
        List<sp_BP_Sel_CustomerBlendRowHeader_Result> GetCustomerBlendRowHeader(int crop, string type);
        List<sp_BP_Sel_PackedToDate_Result> GetPackedToDate(int crop, string type);
        void AddCustomerBlend(List<BD_CustomerBlend> list);
        void EditCustomerBlend(List<BD_CustomerBlend> list);
        void DeleteCustomerBlend(List<BD_CustomerBlend> list);
    }
    public class BlendingPlanBL : IBlendingPlanBL
    {
        private UnitOfWork _unitOfWork;
        public BlendingPlanBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void AddCustomerBlend(List<BD_CustomerBlend> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CustomerBlendRepository.Add(item);
                }

                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public void DeleteCustomerBlend(List<BD_CustomerBlend> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CustomerBlendRepository.Remove(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public void EditCustomerBlend(List<BD_CustomerBlend> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CustomerBlendRepository.Update(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public List<sp_BP_Sel_CustomerBlendColumn_Result> GetCustomerBlendColumn(int crop, string type)
        {
            try
            {
                StoreProcedureRepository repo = new StoreProcedureRepository();
                return repo.GetCustomerBlendColumn(crop, type);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public List<sp_BP_Sel_CustomerBlendRowHeader_Result> GetCustomerBlendRowHeader(int crop, string type)
        {
            try
            {
                StoreProcedureRepository repo = new StoreProcedureRepository();
                return repo.GetCustomerBlendRowHeader(crop, type);
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public List<sp_BP_Sel_PackedToDate_Result> GetPackedToDate(int crop, string type)
        {
            try
            {
                StoreProcedureRepository repo = new StoreProcedureRepository();
                return repo.GetPackedToDate(crop, type);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public List<BD_CustomerBlend> GetCustomerBlend(int crop, string type)
        {
            try
            {
                return _unitOfWork.CustomerBlendRepository
                    .Query(cb => cb.BD_CropSizeDetail.BD_CropSize.Crop == crop &&
                    cb.BD_CropSizeDetail.BD_CropSize.TobaccoType == type)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
