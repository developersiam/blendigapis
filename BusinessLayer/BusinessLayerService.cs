﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{
    public static class BusinessLayerService
    {
        public static IBlendingPlanBL BlendingPlanBL()
        {
            IBlendingPlanBL obj = new BlendingPlanBL();
            return obj;
        }
        public static ICropBL CropBL()
        {
            ICropBL obj = new CropBL();
            return obj;
        }
        public static ICropSizeBL CropSizeBL()
        {
            ICropSizeBL obj = new CropSizeBL();
            return obj;
        }
        public static ICropSizeDetailBL CropSizeDetailBL()
        {
            ICropSizeDetailBL obj = new CropSizeDetailBL();
            return obj;
        }
        public static ICustomerBL CustomerBL()
        {
            ICustomerBL obj = new CustomerBL();
            return obj;
        }
        public static ICustomerSpecBL CustomerSpecBL()
        {
            ICustomerSpecBL obj = new CustomerSpecBL();
            return obj;
        }
        public static IPackedStockBL PackedStockBL()
        {
            IPackedStockBL obj = new PackedStockBL();
            return obj;
        }
        public static IPackedToDateBL PackedToDateBL()
        {
            IPackedToDateBL obj = new PackedToDateBL();
            return obj;
        }
        public static ITypeBL TypeBL()
        {
            ITypeBL obj = new TypeBL();
            return obj;
        }
        public static IPackedStockNetKgBL PackedStockNetKgBL()
        {
            IPackedStockNetKgBL obj = new PackedStockNetKgBL();
            return obj;
        }
        public static IPackedStockDetailBL PackedStockDetailBL()
        {
            IPackedStockDetailBL obj = new PackedStockDetailBL();
            return obj;
        }
        public static ICustomerBlendFilterBL CustomerBlendFilterBL()
        {
            ICustomerBlendFilterBL obj = new CustomerBlendFilterBL();
            return obj;
        }
        public static ICustomerSpecFilterBL CustomerSpecFilterBL()
        {
            ICustomerSpecFilterBL obj = new CustomerSpecFilterBL();
            return obj;
        }
        public static IUserRoleBL UserRoleBL()
        {
            IUserRoleBL obj = new UserRoleBL();
            return obj;
        }
        public static IStockOnHandBL StockOnHandBL()
        {
            IStockOnHandBL obj = new StockOnHandBL();
            return obj;
        }
    }
}
