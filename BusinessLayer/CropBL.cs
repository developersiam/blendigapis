﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface ICropBL
    {
        List<BD_Crop> GetCrops();
        BD_Crop GetCrop(int crop);
        BD_Crop GetDefaultCrop();
        bool IsCrop(int crop);
        void AddCrop(string createUser);
        void SetDefaultCrop(int crop);
        void DeleteCrop(int crop);
    }

    public class CropBL : ICropBL
    {
        private UnitOfWork _unitOfWork;
        public CropBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void AddCrop(string createUser)
        {
            try
            {
                var lastCrop = _unitOfWork.CropRepository.Get().Max(c => c.Crop);
                _unitOfWork.CropRepository.Add(new BD_Crop
                {
                    Crop = lastCrop + 1,
                    DefaultCrop = false,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public void DeleteCrop(int crop)
        {
            try
            {
                var _crop = _unitOfWork.CropRepository.GetSingle(c => c.Crop == crop);
                if (_crop == null)
                    throw new ArgumentNullException("Find not found.");

                _unitOfWork.CropRepository.Remove(_crop);
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BD_Crop GetCrop(int crop)
        {
            try
            {
                return _unitOfWork.CropRepository.GetSingle(c => c.Crop == crop);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public List<BD_Crop> GetCrops()
        {
            try
            {
                return _unitOfWork.CropRepository.Get();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public BD_Crop GetDefaultCrop()
        {
            try
            {
                return _unitOfWork.CropRepository.Query(c => c.DefaultCrop == true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public bool IsCrop(int crop)
        {
            if (_unitOfWork.CropRepository.GetSingle(c => c.Crop == crop) == null)
                return false;
            else
                return true;
        }
        public void SetDefaultCrop(int crop)
        {
            try
            {
                foreach (var item in this.GetCrops())
                {
                    if (item.Crop == crop)
                        item.DefaultCrop = true;
                    else
                        item.DefaultCrop = false;

                    _unitOfWork.CropRepository.Update(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
