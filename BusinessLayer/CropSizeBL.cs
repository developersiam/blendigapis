﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface ICropSizeBL
    {
        void AddCropSize(BD_CropSize val);
        void EditCropSize(BD_CropSize val);
        void DeleteCropSize(Guid cropSizeID);
        BD_CropSize GetCropSizeByID(Guid cropSizeID);
        List<BD_CropSize> GetAllCropSize();
        BD_CropSize GetCropSizeByCropAndType(int crop, string type);
    }
    public class CropSizeBL : ICropSizeBL
    {
        private UnitOfWork _unitOfWork;

        public CropSizeBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void AddCropSize(BD_CropSize val)
        {
            try
            {
                _unitOfWork.CropSizeRepository.Add(val);
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void DeleteCropSize(Guid cropSizeID)
        {
            try
            {
                _unitOfWork.CropSizeRepository.Remove(_unitOfWork.CropSizeRepository
                    .GetSingle(c => c.CropSizeID == cropSizeID));
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void EditCropSize(BD_CropSize val)
        {
            try
            {
                _unitOfWork.CropSizeRepository.Update(val);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_CropSize> GetAllCropSize()
        {
            try
            {
                return _unitOfWork.CropSizeRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BD_CropSize GetCropSizeByID(Guid cropSizeID)
        {
            try
            {
                return _unitOfWork.CropSizeRepository.GetSingle(cs => cs.CropSizeID == cropSizeID);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BD_CropSize GetCropSizeByCropAndType(int crop, string type)
        {
            try
            {
                return _unitOfWork.CropSizeRepository.GetSingle(cs => cs.Crop == crop && cs.TobaccoType == type);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
