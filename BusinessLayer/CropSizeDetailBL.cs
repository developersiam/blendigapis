﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface ICropSizeDetailBL
    {
        List<BD_CropSizeDetail> GetCropSizeDetail(int crop, string type);
        void AddCropSizeDetail(List<BD_CropSizeDetail> list);
        void EditCropSizeDetail(List<BD_CropSizeDetail> list);
        void DeleteCropSizeDetail(List<BD_CropSizeDetail> list);
    }
    public class CropSizeDetailBL : ICropSizeDetailBL
    {
        private UnitOfWork _unitOfWork;

        public CropSizeDetailBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void AddCropSizeDetail(List<BD_CropSizeDetail> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CropSizeDetailRepository.Add(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void DeleteCropSizeDetail(List<BD_CropSizeDetail> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CropSizeDetailRepository.Remove(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void EditCropSizeDetail(List<BD_CropSizeDetail> list)
        {
            try
            {
                foreach (var item in list)
                {
                    _unitOfWork.CropSizeDetailRepository.Update(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_CropSizeDetail> GetCropSizeDetail(int crop, string type)
        {
            try
            {
                return _unitOfWork.CropSizeDetailRepository.Query(csd => csd.BD_CropSize.Crop == crop &&
                csd.BD_CropSize.TobaccoType == type)
                .ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
