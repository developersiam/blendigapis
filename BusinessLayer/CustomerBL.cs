﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface ICustomerBL
    {
        List<customer> GetCustomers();
    }
    public class CustomerBL : ICustomerBL
    {
        private UnitOfWork _unitOfWork;

        public CustomerBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public List<customer> GetCustomers()
        {
            try
            {
                return _unitOfWork.customerRepository.Get().OrderBy(x => x.code).ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
