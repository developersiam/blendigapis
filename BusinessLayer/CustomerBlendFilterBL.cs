﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface ICustomerBlendFilterBL
    {
        List<BD_CustomerBlendFilter> GetByCropAndType(int crop, string type);
        void Add(Guid cropSizeDetailId);
        void Delete(Guid cropSizeDetailID);
    }

    public class CustomerBlendFilterBL : ICustomerBlendFilterBL
    {
        private UnitOfWork _unitOfWork;

        public CustomerBlendFilterBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(Guid cropSizeDetailId)
        {
            try
            {
                _unitOfWork.CustomerBlendFilterRepository.Add(new BD_CustomerBlendFilter { CropSizeDetailID = cropSizeDetailId });
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Delete(Guid cropSizeDetailID)
        {
            try
            {
                BD_CustomerBlendFilter model = new BD_CustomerBlendFilter();
                model = _unitOfWork.CustomerBlendFilterRepository
                    .GetSingle(m => m.CropSizeDetailID == cropSizeDetailID);

                if (model == null) throw new ArgumentNullException("Find not found.");

                _unitOfWork.CustomerBlendFilterRepository.Remove(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_CustomerBlendFilter> GetByCropAndType(int crop, string type)
        {
            try
            {
                return _unitOfWork.CustomerBlendFilterRepository
                    .Get(cb => cb.BD_CropSizeDetail.BD_CropSize.Crop == crop &&
                    cb.BD_CropSizeDetail.BD_CropSize.TobaccoType == type,
                    null,
                    cb => cb.BD_CropSizeDetail,
                    cb => cb.BD_CropSizeDetail.BD_CropSize)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
