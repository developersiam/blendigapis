﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface ICustomerSpecBL
    {
        List<BD_CustomerSpec> GetCustomerSpecByCropAndType(int crop, string type);
        BD_CustomerSpec GetCustomerSpecById(Guid customerSpecID);
        bool IsDupplicate(int crop, string type, string customer, string packedGrade);
        void AddCustomerSpec(BD_CustomerSpec val);
        void EditCustomerSpec(List<BD_CustomerSpec> val);
        void DeleteCustomerSpec(Guid customerSpecID);
        void EditCustomerSpecByItem(BD_CustomerSpec val);
    }

    public class CustomerSpecBL : ICustomerSpecBL
    {
        private UnitOfWork _unitOfWork;

        public CustomerSpecBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void AddCustomerSpec(BD_CustomerSpec val)
        {
            try
            {
                var model = _unitOfWork.CustomerSpecRepository.GetSingle(x => x.PackedGrade == val.PackedGrade);

                if (model != null)
                    throw new ArgumentException("Packgrade " + val.PackedGrade + " is dupplicate.");

                val.CustSpecID = Guid.NewGuid();
                val.CreateDate = DateTime.Now;
                val.ModifiedBy = val.CreateBy;
                val.ModifiedDate = DateTime.Now;

                _unitOfWork.CustomerSpecRepository.Add(val);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void DeleteCustomerSpec(Guid customerSpecID)
        {
            try
            {
                var custSpec = _unitOfWork.CustomerSpecRepository.GetSingle(cs => cs.CustSpecID == customerSpecID);

                var custSpecFilter = _unitOfWork.CustomerSpecFilterRepository.Get(x => x.CustomerSpecID == customerSpecID);
                if (custSpecFilter.Count > 0)
                    throw new ArgumentException("This item was use in a customer specification filter.");

                var custBlends = _unitOfWork.CustomerBlendRepository.Get(x => x.CustSpecID == customerSpecID);
                if (custBlends.Count > 0)
                    throw new ArgumentException("This item was use in a customer blend percentages.");

                _unitOfWork.CustomerSpecRepository.Remove(custSpec);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void EditCustomerSpec(List<BD_CustomerSpec> val)
        {
            try
            {
                List<BD_CustomerSpec> list = new List<BD_CustomerSpec>();
                list = GetCustomerSpecByCropAndType(val.FirstOrDefault().Crop, val.FirstOrDefault().TobaccoType);
                foreach (var item in list)
                {
                    item.YieldManual = val.SingleOrDefault(cs => cs.CustSpecID == item.CustSpecID).YieldManual;
                    item.ModifiedDate = DateTime.Now;
                    _unitOfWork.CustomerSpecRepository.Update(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void EditCustomerSpecByItem(BD_CustomerSpec val)
        {
            try
            {
                var model = GetCustomerSpecById(val.CustSpecID);
                if (model == null)
                    throw new ArgumentNullException("Find not found!");

                _unitOfWork.CustomerSpecRepository.Update(val);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_CustomerSpec> GetCustomerSpecByCropAndType(int crop, string type)
        {
            try
            {
                return _unitOfWork.CustomerSpecRepository.Query(css => css.Crop == crop && css.TobaccoType == type).ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BD_CustomerSpec GetCustomerSpecById(Guid customerSpecID)
        {
            try
            {
                return _unitOfWork.CustomerSpecRepository.GetSingle(c => c.CustSpecID == customerSpecID);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public bool IsDupplicate(int crop, string type, string customer, string packedGrade)
        {
            if (_unitOfWork.CustomerSpecRepository
                .Query(cs => cs.Crop == crop &&
                cs.TobaccoType == type &&
                cs.CustomerCode == customer &&
                cs.PackedGrade == packedGrade).Count() > 0)
                return true;
            else
                return false;
        }
    }
}