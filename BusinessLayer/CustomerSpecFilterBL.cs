﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface ICustomerSpecFilterBL
    {
        List<BD_CustomerSpecFilter> GetByCropAndType(int crop, string type);
        void Add(Guid customerSpecId);
        void Delete(Guid customerSpecID);
    }

    public class CustomerSpecFilterBL : ICustomerSpecFilterBL
    {
        private UnitOfWork _unitOfWork;

        public CustomerSpecFilterBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(Guid customerSpecId)
        {
            try
            {
                BD_CustomerSpecFilter model = new BD_CustomerSpecFilter();
                model.CustomerSpecID = customerSpecId;

                _unitOfWork.CustomerSpecFilterRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Delete(Guid customerSpecID)
        {
            try
            {
                BD_CustomerSpecFilter model = new BD_CustomerSpecFilter();
                model = _unitOfWork.CustomerSpecFilterRepository.GetSingle(m => m.CustomerSpecID == customerSpecID);

                if (model == null) throw new ArgumentNullException("Find not found.");

                _unitOfWork.CustomerSpecFilterRepository.Remove(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_CustomerSpecFilter> GetByCropAndType(int crop, string type)
        {
            try
            {
                return _unitOfWork.CustomerSpecFilterRepository
                    .Get(cs => cs.BD_CustomerSpec.Crop == crop &&
                    cs.BD_CustomerSpec.TobaccoType == type,
                    null,
                    cs => cs.BD_CustomerSpec)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
