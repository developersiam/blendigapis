﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface IPackedStockBL
    {
        void AddPackedStockDetail(Guid customerSpecID, BD_PackedStockDetail val);
        void EditPackedStockDetail(BD_PackedStockDetail val);
        void DeletePackedStockDetail(Guid packedStockDetailID);
        List<BD_PackedStockDetail> GetPackedStockGroupByCustomerSpec(int crop, string type);
    }
    public class PackedStockBL : IPackedStockBL
    {
        private UnitOfWork _unitOfWork;

        public PackedStockBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void AddPackedStockDetail(Guid customerSpecID, BD_PackedStockDetail val)
        {
            throw new NotImplementedException();
        }

        public void DeletePackedStockDetail(Guid packedStockDetailID)
        {
            throw new NotImplementedException();
        }

        public void EditPackedStockDetail(BD_PackedStockDetail val)
        {
            throw new NotImplementedException();
        }

        public List<BD_PackedStockDetail> GetPackedStockGroupByCustomerSpec(int crop, string type)
        {
            throw new NotImplementedException();
        }
    }
}
