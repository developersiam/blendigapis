﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IPackedStockDetailBL
    {
        void Add(BD_PackedStockDetail model);
        void Edit(BD_PackedStockDetail model);
        void Delete(Guid id);
        List<BD_PackedStockDetail> GetListByCustomerSpec(Guid cusSpecId);
        BD_PackedStockDetail GetSingle(Guid id);
        bool IsDupplicate(Guid packedCasesNetKgID, Guid custSpecID);
    }
    public class PackedStockDetailBL : IPackedStockDetailBL
    {
        private UnitOfWork _unitOfWork;
        public PackedStockDetailBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public bool IsDupplicate(Guid packedCasesNetKgID, Guid custSpecID)
        {
            try
            {
                var query = _unitOfWork.PackedStockDetailRepository
                    .Query(p => p.CustSpecID == custSpecID
                    && p.PackedCasesNetKgID == packedCasesNetKgID);

                if (query.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public void Add(BD_PackedStockDetail model)
        {
            try
            {
                if (IsDupplicate(model.PackedCasesNetKgID, model.CustSpecID))
                    throw new ArgumentException("Dupplicate data!");

                _unitOfWork.PackedStockDetailRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var query = _unitOfWork.PackedStockDetailRepository.GetSingle(p => p.PackedStockDetailID == id);
                if (query == null)
                    throw new ArgumentNullException("Find data not found.");

                _unitOfWork.PackedStockDetailRepository.Remove(query);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Edit(BD_PackedStockDetail model)
        {
            try
            {
                var query = _unitOfWork.PackedStockDetailRepository
                    .GetSingle(p => p.PackedStockDetailID == model.PackedStockDetailID);
                if (query == null)
                    throw new ArgumentNullException("Find data not found.");

                model.ModifiedDate = DateTime.Now;
                _unitOfWork.PackedStockDetailRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_PackedStockDetail> GetListByCustomerSpec(Guid cusSpecId)
        {
            try
            {
                return _unitOfWork.PackedStockDetailRepository
                        .Get(p => p.CustSpecID == cusSpecId,
                        null,
                        pd => pd.BD_PackedCasesNetKg)
                        .ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BD_PackedStockDetail GetSingle(Guid id)
        {
            try
            {
                return _unitOfWork.PackedStockDetailRepository.GetSingle(p => p.PackedStockDetailID == id);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
