﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface IPackedStockNetKgBL
    {
        void Add(BD_PackedCasesNetKg model);
        void Edit(BD_PackedCasesNetKg model);
        void Delete(Guid id);
        List<BD_PackedCasesNetKg> GetAll();
        BD_PackedCasesNetKg Get(Guid id);
        bool IsDupplicate(decimal netKg);
    }
    public class PackedStockNetKgBL : IPackedStockNetKgBL
    {
        private UnitOfWork _unitOfWork;
        public PackedStockNetKgBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(BD_PackedCasesNetKg model)
        {
            try
            {
                var data = _unitOfWork.PackedCasesNetKgRepository.Get(p => p.NetKg == model.NetKg);
                if (data.Count > 0) throw new ArgumentException("Net kg " + model.NetKg + " is dupplicate in a system.");

                _unitOfWork.PackedCasesNetKgRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var data = _unitOfWork.PackedCasesNetKgRepository.GetSingle(p => p.PackedCasesNetKgID == id);
                if (data == null) throw new ArgumentNullException("Find data not found.");

                _unitOfWork.PackedCasesNetKgRepository.Remove(data);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public void Edit(BD_PackedCasesNetKg model)
        {
            try
            {
                var data = _unitOfWork.PackedCasesNetKgRepository.GetSingle(p => p.PackedCasesNetKgID == model.PackedCasesNetKgID);
                if (data == null) throw new ArgumentNullException("Find data not found.");

                data = model;
                data.ModifiedDate = DateTime.Now;
                _unitOfWork.PackedCasesNetKgRepository.Update(data);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public BD_PackedCasesNetKg Get(Guid id)
        {
            try
            {
                return _unitOfWork.PackedCasesNetKgRepository.GetSingle(p => p.PackedCasesNetKgID == id);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<BD_PackedCasesNetKg> GetAll()
        {
            try
            {
                return _unitOfWork.PackedCasesNetKgRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public bool IsDupplicate(decimal netKg)
        {
            if (_unitOfWork.PackedCasesNetKgRepository.Query(p => p.NetKg == netKg).Count() > 0)
                return true;
            else
                return false;
        }
    }
}
