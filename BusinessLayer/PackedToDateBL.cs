﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface IPackedToDateBL
    {
        List<sp_BP_Sel_PackedToDate_Result> GetPackToDateByCropAndType(int crop, string type);
    }
    public class PackedToDateBL : IPackedToDateBL
    {
        public List<sp_BP_Sel_PackedToDate_Result> GetPackToDateByCropAndType(int crop, string type)
        {
            try
            {
                StoreProcedureRepository repo = new StoreProcedureRepository();
                return repo.GetPackedToDate(crop, type);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
