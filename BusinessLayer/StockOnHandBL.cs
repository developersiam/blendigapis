﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer
{
    public interface IStockOnHandBL
    {
        List<sp_BP_Sel_StockOnHand_Result> GetStockOnHand(int crop, string type);
    }
    public class StockOnHandBL : IStockOnHandBL
    {
        public List<sp_BP_Sel_StockOnHand_Result> GetStockOnHand(int crop, string type)
        {
            try
            {
                StoreProcedureRepository repo = new StoreProcedureRepository();
                return repo.GetStockOnHand(crop, type);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
