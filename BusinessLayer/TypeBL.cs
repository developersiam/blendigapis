﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer
{
    public interface ITypeBL
    {
        List<type> GetTypes();
    }
    public class TypeBL : ITypeBL
    {
        private UnitOfWork _unitOfWork;

        public TypeBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public List<type> GetTypes()
        {
            try
            {
                return _unitOfWork.typeRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
