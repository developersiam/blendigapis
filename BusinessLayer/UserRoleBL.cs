﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using DomainModel;

namespace BusinessLayer
{
    public interface IUserRoleBL
    {
        List<BD_UserRole> GetRoles(string username);
    }
    public class UserRoleBL : IUserRoleBL
    {
        private UnitOfWork _unitOfWork;
        public UserRoleBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public List<BD_UserRole> GetRoles(string username)
        {
            try
            {
                return _unitOfWork.UserRoleRepository.Get(x => x.UserName == username, null, x => x.BD_Role).ToList();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
