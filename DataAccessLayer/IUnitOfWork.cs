﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public interface IUnitOfWork
    {
        IGenericDataRepository<BD_Crop> CropRepository { get; }
        IGenericDataRepository<BD_CropSize> CropSizeRepository { get; }
        IGenericDataRepository<BD_CropSizeDetail> CropSizeDetailRepository { get; }
        IGenericDataRepository<BD_CustomerBlend> CustomerBlendRepository { get; }
        IGenericDataRepository<BD_CustomerSpec> CustomerSpecRepository { get; }
        IGenericDataRepository<BD_PackedCasesNetKg> PackedCasesNetKgRepository { get; }
        IGenericDataRepository<BD_PackedStockDetail> PackedStockDetailRepository { get; }
        IGenericDataRepository<BD_CustomerBlendFilter> CustomerBlendFilterRepository { get; }
        IGenericDataRepository<BD_CustomerSpecFilter> CustomerSpecFilterRepository { get; }
        IGenericDataRepository<BD_Role> RoleRepository { get; }
        IGenericDataRepository<BD_UserRole> UserRoleRepository { get; }
        IGenericDataRepository<customer> customerRepository { get; }
        IGenericDataRepository<type> typeRepository { get; }
        IGenericDataRepository<pd> pdRepository { get; }
        void Save();
    }
}
