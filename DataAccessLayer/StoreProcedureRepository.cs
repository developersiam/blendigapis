﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DataAccessLayer
{
    public interface IStoreProcedureRepository
    {
        List<sp_BP_Sel_CustomerBlendColumn_Result> GetCustomerBlendColumn(int crop, string type);
        List<sp_BP_Sel_CustomerBlendRowHeader_Result> GetCustomerBlendRowHeader(int crop, string type);
        List<sp_BP_Sel_PackedToDate_Result> GetPackedToDate(int crop, string type);
        List<sp_BP_Sel_StockOnHand_Result> GetStockOnHand(int crop, string type);
    }

    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public List<sp_BP_Sel_CustomerBlendColumn_Result> GetCustomerBlendColumn(int crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_BP_Sel_CustomerBlendColumn(crop, type).ToList();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_BP_Sel_CustomerBlendRowHeader_Result> GetCustomerBlendRowHeader(int crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_BP_Sel_CustomerBlendRowHeader(crop, type).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_BP_Sel_PackedToDate_Result> GetPackedToDate(int crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_BP_Sel_PackedToDate(crop, type).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_BP_Sel_StockOnHand_Result> GetStockOnHand(int crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_BP_Sel_StockOnHand(crop, type).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}