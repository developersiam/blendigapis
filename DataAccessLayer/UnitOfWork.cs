﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using System.Data.Entity;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;
        private IGenericDataRepository<BD_Crop> _cropRepository;
        private IGenericDataRepository<BD_CropSize> _cropSizeRepository;
        private IGenericDataRepository<BD_CropSizeDetail> _cropSizeDetailRepository;
        private IGenericDataRepository<BD_CustomerBlend> _customerBlendRepository;
        private IGenericDataRepository<BD_CustomerSpec> _customerSpecRepository;
        private IGenericDataRepository<BD_PackedCasesNetKg> _packedCasesNetKgrRepository;
        private IGenericDataRepository<BD_PackedStockDetail> _packedStockDetailRepository;
        private IGenericDataRepository<BD_CustomerBlendFilter> _customerBlendFilterRepository;
        private IGenericDataRepository<BD_CustomerSpecFilter> _customerSpecFilterRepository;
        private IGenericDataRepository<BD_Role> _roleRepository;
        private IGenericDataRepository<BD_UserRole> _userRoleRepository;
        private IGenericDataRepository<customer> _customerRepository;
        private IGenericDataRepository<type> _typeRepository;
        private IGenericDataRepository<pd> _pdRepository;

        public UnitOfWork()
        {
            _context = new StecDBMSEntities();
        }
        
        public IGenericDataRepository<BD_Crop> CropRepository
        {
            get
            {
                return _cropRepository ?? (_cropRepository = new GenericDataRepository<BD_Crop>(_context));
            }
        }

        public IGenericDataRepository<BD_CropSize> CropSizeRepository
        {
            get
            {
                return _cropSizeRepository ?? (_cropSizeRepository = new GenericDataRepository<BD_CropSize>(_context));
            }
        }

        public IGenericDataRepository<BD_CropSizeDetail> CropSizeDetailRepository
        {
            get
            {
                return _cropSizeDetailRepository ?? (_cropSizeDetailRepository = new GenericDataRepository<BD_CropSizeDetail>(_context));
            }
        }

        public IGenericDataRepository<BD_CustomerBlend> CustomerBlendRepository
        {
            get
            {
                return _customerBlendRepository ?? (_customerBlendRepository = new GenericDataRepository<BD_CustomerBlend>(_context));
            }
        }

        public IGenericDataRepository<BD_CustomerSpec> CustomerSpecRepository
        {
            get
            {
                return _customerSpecRepository ?? (_customerSpecRepository = new GenericDataRepository<BD_CustomerSpec>(_context));
            }
        }

        public IGenericDataRepository<BD_PackedCasesNetKg> PackedCasesNetKgRepository
        {
            get
            {
                return _packedCasesNetKgrRepository ?? (_packedCasesNetKgrRepository = new GenericDataRepository<BD_PackedCasesNetKg>(_context));
            }
        }

        public IGenericDataRepository<BD_PackedStockDetail> PackedStockDetailRepository
        {
            get
            {
                return _packedStockDetailRepository ?? (_packedStockDetailRepository = new GenericDataRepository<BD_PackedStockDetail>(_context));
            }
        }

        public IGenericDataRepository<BD_CustomerSpecFilter> CustomerSpecFilterRepository
        {
            get
            {
                return _customerSpecFilterRepository ?? (_customerSpecFilterRepository = new GenericDataRepository<BD_CustomerSpecFilter>(_context));
            }
        }

        public IGenericDataRepository<BD_CustomerBlendFilter> CustomerBlendFilterRepository
        {
            get
            {
                return _customerBlendFilterRepository ?? (_customerBlendFilterRepository = new GenericDataRepository<BD_CustomerBlendFilter>(_context));
            }
        }

        public IGenericDataRepository<BD_Role> RoleRepository
        {
            get
            {
                return _roleRepository ?? (_roleRepository = new GenericDataRepository<BD_Role>(_context));
            }
        }

        public IGenericDataRepository<BD_UserRole> UserRoleRepository
        {
            get
            {
                return _userRoleRepository ?? (_userRoleRepository = new GenericDataRepository<BD_UserRole>(_context));
            }
        }

        public IGenericDataRepository<customer> customerRepository
        {
            get
            {
                return _customerRepository ?? (_customerRepository = new GenericDataRepository<customer>(_context));
            }
        }

        public IGenericDataRepository<type> typeRepository
        {
            get
            {
                return _typeRepository ?? (_typeRepository = new GenericDataRepository<type>(_context));
            }
        }

        public IGenericDataRepository<pd> pdRepository
        {
            get
            {
                return _pdRepository ?? (_pdRepository = new GenericDataRepository<pd>(_context));
            }
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}