//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class BD_CropSizeDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BD_CropSizeDetail()
        {
            this.BD_CustomerBlend = new HashSet<BD_CustomerBlend>();
        }
    
        public System.Guid CropSizeDetailID { get; set; }
        public string Classify { get; set; }
        public decimal CropThrow { get; set; }
        public System.Guid CropSizeID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual BD_CropSize BD_CropSize { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BD_CustomerBlend> BD_CustomerBlend { get; set; }
        public virtual BD_CustomerBlendFilter BD_CustomerBlendFilter { get; set; }
    }
}
